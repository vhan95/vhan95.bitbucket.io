/*
 * Create #info elements
 */
const GenerateInfo = (obj) => {
  /*
   * Generate Info column on the left
   */
  const ellipsisObj = {
    'tag': 'p',
    'attribute': {'style': 'text-align: center;color: rgb(87,136,142)'},
    'content': ['Hover to See More'],
  };
  OverflowController(obj.profile.introduction, 410, ellipsisObj).map(
    (x) => $('#profileContent').append(GenerateTag(x))
  );

  /*
   * Generate Contact div on the left
   */
  Object.keys(fill_obj).map((key) => {
    const icon_size = 35;
    const icon_width = icon_size * 1.5;
    const icon_div_padding = (obj.contact[fill_obj[key].id].length - 1) * icon_size / 2;
    $('#contactContent').append(GenerateTag({
      'tag': 'div',
      'attribute': { 'id': key },
      'content': [
        {
          'tag': 'div',
          'attribute': {
            'style': 'width: ' + icon_width + 'px;padding-top: ' + icon_div_padding + 'px;'
          },
          'content': [{
            'tag': 'i',
            'attribute': {
              'class': 'fa ' + key,
              'style': 'font-size:' + icon_size + 'px;color: BLACK;padding: 0'
            },
          },],
        },
        {
          'tag': 'div',
          'attribute': { 'style': 'padding: 0;flex-direction: column' },
          'content': obj.contact[fill_obj[key].id].map((x) => {
            return {
              'tag': 'p',
              'attribute': { 'style': 'line-height: ' + icon_size + 'px;padding: 0' },
              'content': [x],
            };
          }),
        },
      ],
    }));
  });
}


let fill_obj = {
  'fa-whatsapp': { 'id': 'phone', 'split': '', },
  'fa-envelope': { 'id': 'email', 'split': '', },
  'fa-facebook-square': { 'id': 'fb', 'split': '', },
  'fa-github': { 'id': 'git', 'split': '', },
}
