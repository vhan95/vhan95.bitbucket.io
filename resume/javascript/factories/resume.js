async function profileDiv(x) {
  this.container = { 'tag': 'div', 'attribute': { 'id': 'Profile' }, };
  this.title = { 'tag': 'h1', 'content': [ 'Profile' ], };
  try{
    this.content = {
      'tag': 'div',
      'attribute': { 'style': 'padding: 0;flex-direction: column' },
      'content': OverflowController(
        x,
        470,
        {
          'tag': 'p',
          'attribute': {'style': 'text-align: center;color: rgb(87,136,142)'},
          'content': ['Hover to See More!'],
        }
      )
    };
    return GenerateContainer(this.container,this.title,this.content);
  } catch (err) {
    return 'error';
  }
}

async function contactDiv(x) {
  this.container = { 'tag': 'div', 'attribute': { 'id': 'Profile' }, };
  this.title = { 'tag': 'h1', 'content': [ 'Profile' ], };
  try{
    this.content = {
      'tag': 'div',
      'attribute': { 'style': 'padding: 0;flex-direction: column' },
      'content': OverflowController(
        x,
        470,
        {
          'tag': 'p',
          'attribute': {'style': 'text-align: center;color: rgb(87,136,142)'},
          'content': ['Hover to See More!'],
        }
      )
    };
    return GenerateContainer(this.container,this.title,this.content);
  } catch (err) {
    return 'error';
  }
}
