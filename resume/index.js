function Compiler(obj){
  GenerateInfo(obj);
}

/*
 * Create #info elements
 */
function GenerateInfo(obj) {
  // TODO {Task 0}: Clear info div
  $('#info').html('');
  // END {task 0}

  // TODO {Task 1}: Generate Profile div
  profileDiv(obj.profile.introduction)
    .then((x) => $('#info').append(x));
  // END {task 1}

  // TODO {Task 2}: Generate Contact div
  let xxx = Object.keys(fill_obj).map((key) => {
    const icon_size = 35;
    const icon_width = icon_size * 1.5;
    const icon_div_padding = (obj.contact[fill_obj[key].id].length - 1) * icon_size / 2;
    return {
      'tag': 'div',
      'attribute': { 'id': key },
      'content': [
        {
          'tag': 'div',
          'attribute': {
            'style': 'width: ' + icon_width + 'px;padding-top: ' + icon_div_padding + 'px;'
          },
          'content': [{
            'tag': 'i',
            'attribute': {
              'class': 'fa ' + key,
              'style': 'font-size:' + icon_size + 'px;color: BLACK;padding: 0'
            },
          },],
        },
        {
          'tag': 'div',
          'attribute': { 'style': 'padding: 0;flex-direction: column' },
          'content': obj.contact[fill_obj[key].id].map((x) => {
            return {
              'tag': 'p',
              'attribute': { 'style': 'line-height: ' + icon_size + 'px;padding: 0' },
              'content': [x],
            };
          }),
        },
      ],
    };
  });
  GenerateContainer(
    {
      'tag': 'div',
      'attribute': { 'id': 'Contact' },
    },
    { 'tag': 'h1', 'content': [ 'Contact' ], },
    {
      'tag': 'div',
      'attribute': { 'id': 'contactContent' },
      'content': xxx,
    },
  );
}


let fill_obj = {
  'fa-whatsapp': { 'id': 'phone', 'split': '', },
  'fa-envelope': { 'id': 'email', 'split': '', },
  'fa-facebook-square': { 'id': 'fb', 'split': '', },
  'fa-github': { 'id': 'git', 'split': '', },
}
