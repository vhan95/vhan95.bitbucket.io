function generateRowObjList() {
  var container = {
    'search_method': 'id',
    'value': 'page_content',
  };

  let list_divider = {
    'tag': 'div',
    'attribute': { 'style': 'height: 2.5vh'},
  };

  $.getJSON( "general/routes.json", ( data, textStatus, jqxhr ) => {
    let arr = Object.keys(data.pages).sort().map((key) => generateSingleList(data.pages[key], key));

    GenerateTags(container, arr.reduce(
      (arr, element) => arr.concat(element, list_divider), []
    ));
  });
}

const generateSingleList = (page, key) => {
  return {
    'tag': 'div',
    'attribute': { 'class': 'content_list', },
    'content': [
      {
        'tag': 'div',
        'attribute': { 'class': 'content_list_title', },
        'content': [
          {
            'tag': 'a',
            'attribute': {
              'id': key,
              'class': 'active',
              'href': page.href,
            },
            'content': [ page.title ],
          },
        ],
      },
      {
        'tag': 'div',
        'attribute': { 'class': 'content_list_items', },
        'content': page.content.map((x) => generateListContent(x))
      },
    ],
  };
}

const generateListContent = (obj) => {
  return {
    'tag': 'div',
    'attribute': { 'class': 'content_list_item ' + obj.page_development },
    'content': [
      {
        'tag': 'div',
        'content': [
          {
            'tag': 'img',
            'attribute': { 'src': () => {
              if (obj.img == undefined || obj.img.length == 0)
                return 'https://www.freeiconspng.com/uploads/no-image-icon-6.png';
              else
                return obj.img;
            }},
          },
          { 'tag': 'p', 'content': [ obj.title, ], },
          { 'tag': 'p', 'content': [ obj.desc, ], },
        ],
      },
      {
        'tag': 'div',
        'attribute': { 'class': 'subChap' },
        'content': [
          {
            'tag': 'p',
            'content': [ 'subchaps', ],
          },
        ],
      },
    ]
  }
}
