/*
 * Generate a single tag Object
 * tag_info format
 *  {
 *    'tag': --tag,
 *    'attribute': { --attribute: XX, },  --optional
 *    'content': [ --tag_info/text, ], --optional
 *  }
 */
GenerateTag = (obj) => {
  if (typeof obj === 'string' || obj instanceof String)
    return document.createTextNode(obj);
  else {
    let element = jQuery('<' + obj.tag + '/>', obj.attribute);

    if (obj.content != undefined)
      element.append(obj.content.map((x) => GenerateTag(x)));

    return element;
  }
}

/*
 * Multiple tag Objects
 * tag_arr format
 *  [tag_info,]
 * container_info format
 *  {
 *    'search_method': --class,id,tag,
 *    'value': --id,
 *    'index': --index, --only for get elements
 *  }
 */
function GenerateTags(container, arr){
  var get_div = '';
  switch (container.search_method) {
    case 'class':
      get_div = '.';
      break;
    case 'id':
      get_div = '#';
      break;
  }

  $(get_div + container.value).append(arr.map((x) => GenerateTag(x)));
}

/*
 * Generate Multiple tags
 */
function GenerateMultiTags(container_info, tag_arr) {
  for (var each in tag_arr)
    GenerateTags(container_info, tag_arr[each]);
}

/*
 * Generate Objects which will be use through all pages
 */
function GenerateMultiContent(content){
  for (var cont in content)
    GenerateMultiTags(content[cont].container,content[cont].content);
}

/*
 *
 */
function getPageTitle(routes) {
    const iter = routes[Symbol.iterator]();
    for (let value of iter)
      if (value.id == page_id)
        return value.name;
}




function generateFromJSON(container_info,content){
  var get_div;
  switch (container_info.search_method) {
    case 'class':
      get_div = document.getElementsByClassName(container_info.value)[container_info.index];
      break;
    case 'id':
      get_div = document.getElementById(container_info.value);
      break;
    case 'tag':
      get_div = document.getElementsByTagName(container_info.value)[container_info.index];
      break;
  }
  function Generater(contents){
    for (var content in contents) {
      GenerateTag({
        'tag': 'div',
        'attribute': { 'class': 'content_list', },
        'content': [
          {
            'tag': 'div',
            'content': [

            ],
          },
          {
            'tag': 'div',
            'content': [

            ],
          }
        ],
      });
      GenerateTag({
        'tag': 'div',
        'attribute': { 'style': 'height: 2vh', },
      })
    };
  }
  get_div.appendChild(Generater(content));
}
