/**
 * TODO:
 *  Add the following line in client side html file to import
 *  <script src="javascript/UIGenerator.js"></script>
 * @param {function} GenerateTag
 * @param {function} OverflowController
 */

 /**
  * TODO: Generate a container and contain everything into it
  * @param {object} containerInfoObj
  *  { 'tag': --tag_type, 'attribute': { OPTIONAL } }
  * @param {object} titleObj
  *  { 'tag': --tag_type, 'attribute': { OPTIONAL }, 'content': [ OPTIONAL(string) ], }
  * @param {object} contentObj
  *  { 'tag': --tag_type, 'attribute': { OPTIONAL }, 'content': [ OPTIONAL ], }
  */
function GenerateContainer(containerInfoObj, titleObj={}, contentObj={}){
  const fullDiv = GenerateTag(containerInfoObj);
  const title = GenerateTag(titleObj);
  const content = GenerateTag(contentObj);
  fullDiv.append([title, content]);
  return fullDiv;
}

/**
 * TODO:
 *  Generate a single tag Object
 * @param {string} id
 *  --id_of_div(add_#_before_id)
 */
function GenerateError(id) {
  return 'Error';
}

/**
 * TODO:
 *  Generate a single tag Object
 * @param {object} obj
 *  { 'tag': --tag_type, 'attribute': { OPTIONAL }, 'content': [ OPTIONAL ], }
 */
function GenerateTag(obj) {
  if (typeof obj === 'object' || obj instanceof Object) {
    const element = jQuery('<' + obj.tag + '/>');

    typeof obj.attribute != 'undefined' ?
      Object.keys(obj.attribute).map(x => element.attr(x, obj.attribute[x])):
      null;

    typeof obj.content != 'undefined' ?
      element.append(obj.content.map((x) => GenerateTag(x))):
      null;

    return element;
  }
  return obj;
}

/**
 * TODO:
 *  Trim long messages to short messages and add ellipsis and message at the end
 * @param {array[string]} messagesObj
 *  [--messages_by_paragraph]
 * @param {int} flag
 *  --total_char_before_cutoff
 * @param {array[object]} ellipsisObj
 *  { 'tag': --tag_type, 'attribute': { OPTIONAL }, 'content': [ OPTIONAL ], }
 * @param {object} seperatorObj
 *  { 'tag': --tag_type, 'attribute': { OPTIONAL }, 'content': [ OPTIONAL ], }
 */
function OverflowController(messagesObj, length_flag, ellipsisObj, seperatorObj = {}) {
  const iter = messagesObj[Symbol.iterator]();
  var cur = iter.next();
  var introduction = [];
  while(!cur.done){
    if (length_flag - cur.value.length > 0) {
      introduction.push({
        'tag': 'p',
        'attribute': {'class': 'preIndent'},
        'content': [cur.value],
      });
      introduction.push(seperatorObj);
    } else {
      introduction.push({
        'tag': 'p',
        'attribute': {'class': 'preIndent'},
        'content': [cur.value.substring(0, length_flag) + ' ...'],
      });
      introduction.push(ellipsisObj);
      break;
    }
    length_flag -= cur.value.length;
    cur = iter.next();
  }
  return introduction;
}
